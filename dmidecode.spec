Name:	 dmidecode
Version: 3.6
Release: 1
Epoch:   1
Summary: DMI data report tool

License: GPL-2.0-or-later
URL:	 https://www.nongnu.org/dmidecode/
Source0: https://download.savannah.gnu.org/releases/dmidecode/%{name}-%{version}.tar.xz

Patch1:  bugfix-compat_uuid.patch

BuildRequires: make gcc xz

ExclusiveArch:  %{ix86} x86_64 ia64 aarch64 amd64 sw_64 loongarch64 riscv64 ppc64le

%description
Dmidecode reports information about your system's hardware as described
in your system BIOS according to the SMBIOS/DMI standard (see a sample
output). This information typically includes system manufacturer, model
name, serial number, BIOS version, asset tag as well as a lot of other
details of varying level of interest and reliability depending on the
manufacturer. This will often include usage status for the CPU sockets,
expansion slots (e.g. AGP, PCI, ISA) and memory module slots, and the
list of I/O ports (e.g. serial, parallel, USB).

DMI data can be used to enable or disable specific portions of kernel code
depending on the specific hardware. Thus, one use of dmidecode is for kernel
developers to detect system "signatures" and add them to the kernel source code
when needed.

%prep
%autosetup -n %{name}-%{version} -p1

%build
# biosdecode ownership vpddecode programs are only useful on x86,
# so Makefile atuo compiled target programs depend on arch.
make %{?_smp_mflags} CFLAGS="%{__global_cflags}" LDFLAGS="%{__global_ldflags}"


%install
%make_install prefix=%{_prefix}


%files
%license LICENSE
%{_sbindir}/*
%{_docdir}/%{name}/*
%{_mandir}/man8/*.8*

%changelog
* Wed Jan 08 2025 Funda Wang <fundawang@yeah.net> - 1:3.6-1
- update to 3.6

* Fri Sep 20 2024 liyunqing <liyunqing@kylinos.cn> - 1:3.5-3
- Type:backport
- ID:NA
- SUG:NA
- DESC: Add support for Loongarch processor architecture

* Thu Sep 12 2024 liyunqing <liyunqing@kylinos.cn> - 1:3.5-2
- backport: Use read_file instead of mem_chunk 

* Mon Feb 5 2024 nicunshu <nicunshu@huawei.com> - 1:3.5-1
- Upgrade to 3.5

* Thu Dec 14 2023 lvgenggeng <lvgenggeng@uniontech.com> - 1:3.4-6
- backport: Fix segmentation fault in dmi_hp_240_attr()

* Thu Dec 14 2023 jiahua.yu <jiahua.yu@shingroup.cn> - 1:3.4-5
- init support for arch ppc64le

* Fri Aug 04 2023 Jingwiw  <wangjingwei@iscas.ac.cn> - 1:3.4-4
- enable riscv64 architecture

* Tue Apr 18 2023 Cunshu Ni <nicunshu@huawei.com> - 1:3.4-3
- fix CVE-2023-30630

* Mon Nov 14 2022 Wenlong Zhang <zhangwenlong@loongson.cn> - 1:3.4-2
- add loongarch support for dmidecode

* Wed Nov 9 2022 fanrui<fary.fanrui@huawei.com> - 1:3.4-1
- Update to 3.4

* Thu Nov 3 2022 wuzx<wuzx1226@qq.com> - 1:3.3-4
- Add sw64 architecture

* Tue Oct 25 2022 fanrui <fary.fanrui@huawei.com> - 1:3.3-4
- rebuild package

* Thu Feb 24 2022 Yang Yanchao <yangyanchao6@huawei.com> - 3.3-3
- dmidecode: Fix crash with -u option

* Sat Jan 29 2022 Yang Yanchao <yangyanchao6@huawei.com> - 3.3-2
- add command --compat-uuid

* Tue Nov 30 2021 zhouwenpei <zhouwenpei1@huawei.com> - 3.3-1
- update to 3.3

* Wed Aug 21  2019 openEuler Buildteam <buildteam@openeuler.org> - 1:3.2-2
- Package init
